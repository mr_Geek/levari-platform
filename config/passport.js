var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy
var JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt ? require('passport-jwt').ExtractJwt : null

var EXPIRES_IN_MINUTES = 60 * 30
var SECRET = process.env.tokenSecret || "4ukI0uIVnB3iI1yxj646fVXSE3ZVk4doZgz6fTbNg7jO41EAtl20J5F7Trtwe7OM"
var ALGORITHM = "HS256"
var ISSUER = "http://localhost:1337"
var AUDIENCE = "http://localhost:1337"

var LOCAL_STRATEGY_COFIG = {
  usernameField: 'email',
  password: 'password',
  passReqToCallback: false
}

var jwtFromRequest = ExtractJwt ? ExtractJwt.fromAuthHeader() : null
var JWT_STRATEGY_CONFIG = {
  jwtFromRequest: jwtFromRequest,
  secretOrKey: SECRET,
  issuer: ISSUER,
  audience: AUDIENCE,
  passReqToCallback: false
}

function _onLocalStrategyAuth(email, password, next) {
  User.findOne({email: email}, function (err, user){
    if (err){
      return next(err, false, [])
    }
    if (!user || user.length < 1) {
      return next(null, false, {
        code: 'E_USER_NOT_FOUND',
        message: 'There is no user in our database with such email: ' + email
      })
    }
    if (!CipherService.comparePassword(password, user)) {
      return next(null, false, {
        code: 'E_WRONG_PASS',
        message: 'Password is not correct, please enter your password again.'
      })
    }
    return next(null, user, {
      code: 'S_USER_LOGGED',
      message: 'Welcome...'
    })
  })
}

function _onJwtStrategyAuth(payload, next) {
  var user = payload.user
  return next(null, user, {
    code: 'S_USER_LOGGED',
    message: 'Hello....'
  })
}

passport.use(new LocalStrategy(LOCAL_STRATEGY_COFIG, _onLocalStrategyAuth))
passport.use(new JwtStrategy(JWT_STRATEGY_CONFIG, _onJwtStrategyAuth))

module.exports.jwtSettings = {
  expiresInMinutes: EXPIRES_IN_MINUTES,
  secret: SECRET,
  algorithm: ALGORITHM,
  issuer: ISSUER,
  audience: AUDIENCE
}
