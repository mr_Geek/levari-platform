/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    controller: 'UserController',
    action: 'homePage'
  },

  '/getHours': {
    controller: 'UserController',
    action: 'getHours_by_user'
  },

  '/createHour': {
    controller: 'UserController',
    action: 'createHour'
  },

  '/createCompany': {
    controller: 'CompanyController',
    action: 'create_company'
  },

  '/getCompanies': {
    controller: 'CompanyController',
    action: 'retrieve_companies'
  },


  '/auth/login': {
    controller: 'AuthController',
    action: 'signin'
  },

  '/auth/refreshToken': {
    controller: 'AuthController',
    action: 'refreshToken'
  },

  'POST /auth/register': {
    controller: 'AuthController',
    action: 'signup'
  },

  '/getUserCompanies': {
    controller: 'CompanyController',
    action: 'get_company_by_user'
  },

  '/getUserStats': {
    controller: 'UserController',
    action: 'getUserStats'
  },

  'POST /auth/server-login': {
    controller: 'AuthController',
    action: 'loginServer'
  },

  '/auth/server-logout': {
    controller: 'AuthController',
    action: 'logoutServer'
  },

  '/requestAccess': {
    controller: 'RequestController',
    action: 'requestAccess'
  },


  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

  /*PANEL NAVIGATION*/
                        '/dashboard': {
                          controller: 'PanelController',
                          action: 'dashboard_home'
                        },

                        '/dashboard/home': {
                          controller: 'PanelController',
                          action: 'dashboard_home'
                        },

                        '/dashboard/profile': {
                          controller: 'PanelController',
                          action: 'profile_page'
                        },

                        '/dashboard/clients': {
                          controller: 'PanelController',
                          action: 'clients_page'
                        },

                        '/dashboard/hours': {
                          controller: 'PanelController',
                          action: 'hours_page'
                        },

                        '/dashboard/all-hours': {
                          controller: 'PanelController',
                          action: 'all_hours_page'
                        },

                        '/dashboard/all-users': {
                          controller: 'PanelController',
                          action: 'all_users_page'
                        },

                        '/dashboard/all-clients': {
                          controller: 'PanelController',
                          action: 'all_clients_page'
                        },

                        '/client/account': {
                          controller: 'PanelController',
                          action: 'client_account_page'
                        },

                        '/client/home': {
                          controller: 'PanelController',
                          action: 'client_home'
                        },

                        '/client/hours': {
                          controller: 'PanelController',
                          action: 'client_hours'
                        },
  /*PANEL NAVIGATION*/


  /*PROCESSES*/



                    /* PROFILE PROCESSES */

                                  'POST /profile/update-profile': {
                                    controller: 'ProfileController',
                                    action: 'update_profile'
                                  },

                                  'POST /profile/update-password': {
                                    controller: 'ProfileController',
                                    action: 'update_password'
                                  },

                    /* PROFILE PROCESSES */



                    /* CLIENT PROCESSES */

                                  'POST /client/add-client': {
                                    controller: 'UserClientsController',
                                    action: 'add_client_process'
                                  },

                                  'GET /client/get-client': {
                                    controller: 'UserClientsController',
                                    action: 'get_client_by_id'
                                  },

                                  'POST /client/edit-client': {
                                    controller: 'UserClientsController',
                                    action: 'edit_client_process'
                                  },

                                  '/client/remove-client': {
                                    controller: 'UserClientsController',
                                    action: 'remove_client_process'
                                  },

                    /* CLIENT PROCESSES */

                    /* HOUR PROCESSES */

                                  'POST /hour/add-hour': {
                                    controller: 'UserHoursController',
                                    action: 'add_hour_process'
                                  },

                                  'GET /hour/get-hour': {
                                    controller: 'UserHoursController',
                                    action: 'get_hour_by_id'
                                  },

                                  'POST /hour/edit-hour': {
                                    controller: 'UserHoursController',
                                    action: 'edit_hour_process'
                                  },

                                  '/hour/remove-hour': {
                                    controller: 'UserHoursController',
                                    action: 'remove_hour_process'
                                  },

                    /* HOUR PROCESSES */


                    /* USER PROCESSES */

                                  '/user/get-hour-user': {
                                    controller: 'UserHoursController',
                                    action: 'get_hour_user'
                                  },

                    /* USER PROCESSES */

                    /* ADMIN PROCESSES */

                                  'POST /user/add-user': {
                                    controller: 'AdminUserController',
                                    action: 'add_user'
                                  },

                                  '/user/remove-user': {
                                    controller: 'AdminUserController',
                                    action: 'remove_user_process'
                                  },

                                  'POST /auth/show-client-password': {
                                    controller: 'AuthController',
                                    action: 'show_client_password'
                                  },

                                  'POST /user/add_client': {
                                    controller: 'AdminUserController',
                                    action: 'add_client_process'
                                  },

                                  '/user/remove-client': {
                                    controller: 'AdminUserController',
                                    action: 'remove_client_process'
                                  },

                                  '/user/more-details': {
                                    controller: 'AdminUserController',
                                    action: 'more_details_process'
                                  },

                    /* ADMIN PROCESSES */

                    /* CLIENT PROCESSES */

                                  '/client/update-profile': {
                                    controller: 'ClientController',
                                    action: 'client_update_profile'
                                  },

                                  '/client/update-password': {
                                    controller: 'ClientController',
                                    action: 'client_update_password'
                                  },

                    /* CLIENT PROCESSES */

                    /* REQUEST PROCESSES */

                                  '/request/remove-request': {
                                    controller: 'RequestController',
                                    action: 'remove_request'
                                  },


                                  '/request/grant-request': {
                                    controller: 'RequestController',
                                    action: 'grant_request'
                                  },

                    /* REQUEST PROCESSES */

  /*PROCESSES*/

  /* SOCKET PROCESSES */

    '/socket/subscribe-requests': {
      controller: 'RequestController',
      action: 'subscribeToRequest'
    },

  /* SOCKET PROCESSES */


};
