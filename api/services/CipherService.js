/**
* This service holds all the code that does hashing and/or token stuff
*/
var bcrypt = require('bcrypt-nodejs')
var jwt = require('jsonwebtoken')

module.exports = {
  secret: sails.config.jwtSettings.secret,
  issuer: sails.config.jwtSettings.issuer,
  audience: sails.config.jwtSettings.audience,

  /**
  * Hash the password field for the user
  */
  hashPassword: function (user) {
    if (user.password) {
      user.password = bcrypt.hashSync(user.password)
    }
  },

  /**
  * Compare user password hash with the unhashed password
  * @return boolean indicating a match
  */
  comparePassword: function (password, user) {
    return bcrypt.compareSync(password, user.password)
  },  

  /**
  * Create token based on the passed user
  * @param user
  * @return token
  */
  /*createToken: function (user) {
    return jwt.sign(
      {
        user: user.toJSON()
      },
      sails.config.jwtSettings.secret,
      {
        algorithm: sails.config.jwtSettings.algorithm,
        expiresIn: sails.config.jwtSettings.expiresInMinutes,
        issuer: sails.config.jwtSettings.issuer,
        audience: sails.config.jwtSettings.audience
      }
    )
  }*/
  createToken: function (user) {
    return jwt.sign(
      user,
      sails.config.jwtSettings.secret,
      {
        algorithm: sails.config.jwtSettings.algorithm,
        expiresIn: sails.config.jwtSettings.expiresInMinutes,
        issuer: sails.config.jwtSettings.issuer,
        audience: sails.config.jwtSettings.audience
      }
    )
  }
}
