/**
 * Client.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  schema: true,

  attributes: {

  	email: {
  		type: 'string',
  		// email: true,
  		// required: true,
  		unique: true
  	},

  	client_name: {
  		type: 'string',
  		// required: true
		},
		
		granted: {
			type: 'string'
		}

  },

};
