/**
 * Hour.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var moment = require('moment')
let toDate = function(unix) {
	return moment(unix, 'x').format('MM-DD-YYYY hh:mmA')
}
module.exports = {

  attributes: {

    clientName: {
      type: 'string',
      defaultsTo: 'Not Mentioned'
    },

    pricePerHour: {
      type: 'string',
      defaultsTo: '10'
    },

    numberOfHours: {
      type: 'string',
      defaultsTo: '0'
    },

    totalPrice: {
      type: 'string',
      defaultsTo: '0'
    },

    details: {
      type: 'string',
      defaultsTo: ''
    },

    priceCustom: {
      type: 'string'
    },

    chargable: {
      type: 'string'
    },

    startTime: {
      type: 'string'
    },

    endTime: {
      type: 'string'
    },

    user: {
      type: 'string'
    },

    toJSON: function() {
      var obj = this.toObject()
      obj.createdAt = moment(obj.createdAt).format('MM-DD-YYYY hh:mmA')
      obj.updatedAt = moment(obj.updatedAt).format('MM-DD-YYYY hh:mmA')
      obj.startTime = toDate(obj.startTime)
      obj.endTime = toDate(obj.endTime)
  		return obj
    }



  }
};
