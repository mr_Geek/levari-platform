/**
 * Company.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

var moment = require('moment')

module.exports = {

  attributes: {

    email: {
  		type: 'string',
  		email: true,
  		unique: true
  	},

    contact_mail: {
      type: 'string',
  		email: true,
  		unique: true
    },

    password: {
  		type: 'string',
  		required: true
  	},

    randomPassword: {
      type: 'string'
    },

    companyName: {
      type: 'string',
      defaultsTo: ''
    },

    pricePerHour: {
      type: 'string',
      defaultsTo: '0'
    },

    user: {
      type: 'string'
    },

    toJSON: function() {
      var obj = this.toObject()
      obj.createdAt = moment(obj.createdAt).format('MM-DD-YYYY hh:mmA')
  		return obj
    }

  },

  beforeUpdate: function(values, next) {
    CipherService.hashPassword(values)
    next()
  },

  beforeCreate: function(values, next) {
    CipherService.hashPassword(values)
    next()
  }

};
