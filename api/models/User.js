/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */



module.exports = {

  schema: true,

  attributes: {
  	email: {
  		type: 'string',
  		email: true,
  		required: true,
  		unique: true
  	},

  	username: {
  		type: 'string',
  		required: true
  	},

  	password: {
  		type: 'string',
  		required: true
  	},

  	firstName: {
  		type: 'string',
  		required: true
  	},

  	hours: {
  		type: 'objectid'
  	},

    type: {
      type: 'string'
    },

  	toJSON: function() {
  		var obj = this.toObject()
  		delete obj.password
  		return obj
  	}
  },

  beforeUpdate: function(values, next) {
    CipherService.hashPassword(values)
    next()
  },

  beforeCreate: function(values, next) {
    CipherService.hashPassword(values)
    next()
  }

};
