/**
 * PanelController
 *
 * @description :: Server-side logic for managing panels
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	dashboard_home: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type && req.session.user_id) {
			Request.find(function(err, requests) {
				if (err) {
					return res.view('messages/error', {
						code: 'E_RR',
						message: 'Error while retrieving requests, please try again.'
					})
				}
				return res.view('dashboard/home', {
					data: requests
				})
			})
			
		} else {
			return res.forbidden()
		}
	},

	profile_page: function(req, res) {
		console.log(req.isAuthenticated(), req.session.user_type)
		if (req.isAuthenticated() && req.session.user_type) {
			let id = req.param('id') ? req.param('id') : null

			if (id) {
				User.findOne({id: id}, function(err, user) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_UFO_PP',
							message: 'Error retrieving your profie, it is totaly our fault. We are fixing it right now.'
						})
					}
					// If this is the same user wants to view his profile or it is an admin, then view it
					if (req.session.user_id === user.id || req.session.user_type === 'admin') {
						return res.view('dashboard/profile', {
							data: user,
						})
					} else {
						return res.forbidden()
					}
				})
			} else {
				return res.view('messages/error', {
					code: 'E_LPP',
					message: 'Error loading your profile. Most likely it is server side fault, we on it.'
				})
			}

		} else {
			return res.forbidden()
		}
	},

	clients_page: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type) {
			let id = req.param('id') ? req.param('id') : null

			if (id) {
				Company.find({user: id}, function(err, companies) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CP',
							message: 'Error retrieving user companies, it is totaly our fault. We are fixing it right now.'
						})
					}

					return res.view('dashboard/clients', {
						data: companies,
					})
				})
			} else {
				return res.view('messages/error', {
					code: 'E_CPID',
					message: 'Error loading your clients. Most likely it is server side fault, we on it.'
				})
			}

		} else {
			return res.forbidden()
		}
	},

	hours_page: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type) {
			let id = req.param('id') ? req.param('id') : null

			if (id) {
				Hour.find({user: id}, function(err, hours) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CP',
							message: 'Error retrieving user hours, it is totaly our fault. We are fixing it right now.'
						})
					}
					Company.find({user: id}, function(err, companies){
						if (err) {
							return res.view('messages/error', {
								code: 'E_CP',
								message: 'Error retrieving user companies, it is totaly our fault. We are fixing it right now.'
							})
						}
						return res.view('dashboard/hours', {
							data: hours,
							companies: companies
						})
					})
				})
			} else {
				return res.view('messages/error', {
					code: 'E_HPPM',
					message: 'Error loading your clients. Most likely it is server side fault, we on it.'
				})
			}

		} else {
			return res.forbidden()
		}
	},

	all_hours_page: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type && req.session.user_type === 'admin') {
			Hour.find(function(err, hours) {
				if (err) {
					return res.view('messages/error', {
						code: 'E_CP',
						message: 'Error retrieving user hours, it is totaly our fault. We are fixing it right now.'
					})
				}
				return res.view('dashboard/admin/all-hours', {
					data: hours,
				})
			})
		} else {
			return res.forbidden()
		}
	},

	all_users_page: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type && req.session.user_type === 'admin') {
			User.find(function(err, users) {
				if (err) {
					return res.view('messages/error', {
						code: 'E_CP',
						message: 'Error retrieving users, it is totaly our fault. We are fixing it right now.'
					})
				}
				return res.view('dashboard/admin/all-users', {
					data: users,
				})
			})
		} else {
			return res.forbidden()
		}
	},

	all_clients_page: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type && req.session.user_type === 'admin') {
			Company.find(function(err, companies) {
				if (err) {
					return res.view('messages/error', {
						code: 'E_CP',
						message: 'Error retrieving clients, it is totaly our fault. We are fixing it right now.'
					})
				}
				User.find(function(err, users) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CP',
							message: 'Error retrieving users, it is totaly our fault. We are fixing it right now.'
						})
					}
					return res.view('dashboard/admin/all-clients', {
						data: companies,
						users: users
					})
				})
			})
		} else {
			return res.forbidden()
		}
	},

	client_account_page: function(req, res) {
		if (req.isAuthenticated() && req.session.client && req.session.client_id) {
			let id = req.param('id') ? req.param('id') : null

			if (id) {
				Company.findOne({id: id}, function(err, client) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CLIENT_UFO_PP',
							message: 'Error retrieving your profie, it is totaly our fault. We are fixing it right now.'
						})
					}
					if (req.session.client_id === client.id) {

						User.findOne({id: client.user}, function(err, user) {
							if (err) {
								return res.view('messages/error', {
									code: 'E_CLIENT_UFUO_PP',
									message: 'Error retrieving your profie, it is totaly our fault. We are fixing it right now.'
								})
							}

							return res.view('dashboard/client/account', {
								layout: 'dashboard/client/client_layout',
								data: client,
								user: user
							})
						})
					} else {
						return res.forbidden()
					}
				})
			} else {
				return res.view('messages/error', {
					code: 'E_LPP',
					message: 'Error loading your profile. Most likely it is server side fault, we on it.'
				})
			}

		} else {
			return res.forbidden()
		}
	},

	client_home: function(req, res) {
		if (req.isAuthenticated() && req.session.client && req.session.client_id) {
			return res.view('dashboard/client/home', {
				layout: 'dashboard/client/client_layout',
			})
		} else {
			return res.forbidden()
		}
	},

	client_hours: function(req, res) {
		if (req.isAuthenticated() && req.session.client && req.session.client_id) {
			let id = req.param('id') ? req.param('id') : null

			if (id) {
				Company.findOne({id: id}, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_UFO_PP',
							message: 'Error retrieving your data, it is totaly our fault. We are fixing it right now.'
						})
					}
					Hour.find({clientName: company.companyName}, function(err, hours) {
						if (err) {
							return res.view('messages/error', {
								code: 'E_UFO_PP',
								message: 'Error retrieving your data, it is totaly our fault. We are fixing it right now.'
							})
						}
							if (req.session.client_id === company.id) {
							return res.view('dashboard/client/hours', {
								layout: 'dashboard/client/client_layout',
								data: hours,
							})
						} else {
							return res.forbidden()
						}
					})
				})

			} else {
				return res.view('messages/error', {
					code: 'E_LPP',
					message: 'Error loading your data. Most likely it is server side fault, we on it.'
				})
			}

		} else {
			return res.forbidden()
		}
	},

};
