/**
 * RequestController
 *
 * @description :: Server-side logic for managing Requests
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var randomstring = require("randomstring")

module.exports = {
	
		requestAccess: function(req, res) {
			let client_name = req.param('client_name') ? req.param('client_name') : null
			let email = req.param('email') ? req.param('email') : null

			if (client_name && email) {
				let requestModel = {
					client_name: client_name,
					email: email
				}

				Request.create(requestModel, function(err, request) {
					if (err){
						if (err.code === 'E_VALIDATION' && err.invalidAttributes.email) {
							return res.json({
								message: 'This email already registered on our system.'
							})
						}
						return res.json({
							message: 'An error occurred while processing the information you provided. Please try again.'
						})
					}
					Request.publishCreate(request)
					return res.json({
						message: 'We will message you after checking your information, soon.'
					})
				})
			} else {
				return res.json({
					message: 'Error, No parameters were sent.'
				})
			}
		},

		subscribeToRequest: function(req, res) {
			if (req.isSocket) {
				Request.watch(req)
				return res.json({
					message: 'Subscribed to socket'
				})
			} else {
				return res.json({
					message: '404'
				})
			}
		},

		remove_request: function(req, res) {
			if (req.isAuthenticated() && req.session.user_type === 'admin') {
				let id = req.param('id') ? req.param('id') : null

				if (id) {
					Request.destroy({id: id}, function(err, request) {
						if (err){
							return res.json({
								message: 'Error occurred while removing request'
							})
						}
						return res.json({
							request: request,
							message: 'Request has been removed.'
						})
					})
				} else {
					return res.json({
						message: 'Error, No parameters were sent.'
					})
				}
			} else {
				return res.forbidden()
			}
		},

		grant_request: function(req, res) {
			if (req.isAuthenticated() && req.session.user_type === 'admin') {
				var id = req.param('id') ? req.param('id') : null				
				var user_id = req.param('user_id') ? req.param('user_id') : null
				var contact_mail = req.param('email') ? req.param('email') : null
				var companyName = req.param('client_name') ? req.param('client_name') : null
				var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : null

				var email = companyName.toLowerCase().trim().replace(/\s/g,'').replace(/[^a-zA-Z0-9 ]/g, "") + '@levarilaw.com'
				var password = randomstring.generate({
					length: 20,
					charset: 'alphanumeric'
				})

				if (user_id && companyName && pricePerHour && password && email && contact_mail) {
					let companyModel = {
						user: user_id,
						email: email,					
						companyName: companyName,
						pricePerHour: pricePerHour,
						contact_mail: contact_mail,
						password: password,
						randomPassword: password,
					}

					Company.create(companyModel, function(err, company) {
						if (err) {
							return res.view('messages/error', {
								code: 'E_CP',
								message: 'Error granting access to the new client, it is totaly our fault. We are fixing it right now.\n' + err
							})
						}

						Request.destroy({id: id}, function(err, request) {
							if (err){
								return res.view('messages/error', {
									code: 'E_CP',
									message: 'Error while removing request, it is totaly our fault. We are fixing it right now.'
								})
							}
							req.addFlash('success', 'New client has been added successfully')
							return res.redirect('/dashboard')
						})
						
					})
				} else {
					return res.view('messages/error', {
						code: 'E_ACP_PMS',
						message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
					})
				}
				
			} else {
				return res.forbidden()
			}
		},
};

