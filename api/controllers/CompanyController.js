/**
 * CompanyController
 *
 * @description :: Server-side logic for managing companies
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var jwt = require('jsonwebtoken')
var randomstring = require("randomstring")

module.exports = {

	create_company: function(req, res) {

		var authentication = req.headers.authentication ? req.headers.authentication : null

		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {
					if (err) {
						console.log(err)
						return res.json({
							code: 'E_AUTHO',
							message: 'Error occurred, While authenticating.',
							data: false
						})
					}
					var companyName = req.param('companyName') ? req.param('companyName') : null
					var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : null
					var user = decoded.id
					
					if (companyName && pricePerHour && user) {
						var email = companyName.toLowerCase().trim().replace(/\s/g,'').replace(/[^a-zA-Z0-9 ]/g, "") + '@levarilaw.com'
						var password = randomstring.generate({
							length: 20,
							charset: 'alphanumeric'
						})

						var companyModel = {
							companyName: companyName,
							pricePerHour: pricePerHour,
							password: password,
							randomPassword: password,
							email: email,
							user: user
						}
						Company.create(companyModel, function(err, company) {
							if (err){
								console.log(err)
								return res.json({
									code: 'ERROR_CREATE_COMPANY',
									message: 'Sorry, an error occurred while creating company.',
									data: false
								})
							}
							return res.json({
								code: '',
								message: '',
								data: company
							})
						})
					} else {
						return res.json({
							code: 'ERROR_FEW_PARAMS',
							message: 'Please fill all the fields.',
							data: false
						})
					}
				})
			} catch (e) {
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					data: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				data: false
			})
		}
	},

	retrieve_companies: function(req, res) {

		Company.find(function(err, companies) {
			if (err){
				return res.json({
					code: 'ERROR_FIND_COMPANY',
					message: 'Sorry, an error occurred while retrieving companis from the server.',
					data: false
				})
			}
			return res.json({
				code: '',
				message: '',
				data: companies
			})
		})
	},

	get_company_by_id: function(req, res) {

		var id = req.param('id') ? req.param('id') : null

		if (id) {
			Company.findOne({id: id}, function(err, company) {
				if (err){
					return res.json({
						code: 'ERROR_FINDONE_COMPANY',
						message: 'Sorry, an error occurred while retrieving company.',
						data: false
					})
				}
				return res.json({
					code: '',
					message: '',
					data: company
				})
			})
		} else {
			return res.json({
				code: 'ERROR_FEW_PARAMS',
				message: 'Please fill all the fields.',
				data: false
			})
		}
	},

	get_company_by_user: function(req, res) {

		var authentication = req.headers.authentication ? req.headers.authentication : null

		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {
					if (err) {
						console.log('---------------------------')
						console.log('AUTH_ERROR_GET_USER_COMPANIES')
						console.log('---------------------------')
						console.log(err)
						console.log('---------------------------')
						console.log('AUTH_ERROR_GET_USER_COMPANIES')
						console.log('---------------------------')

						return res.json({
							code: 'E_AUTHO',
							message: 'Error occurred, While authenticating.',
							data: false
						})
					}
					if (decoded.id) {
						Company.find({user: decoded.id}, function(err, companies) {
							if (err){
								return res.json({
									code: 'ERROR_FINDONE_COMPANY',
									message: 'Sorry, an error occurred while retrieving companies.',
									data: false
								})
							}
							return res.json({
								code: '',
								message: '',
								data: companies
							})
						})
					} else {
						return res.json({
							code: 'ERROR_JWT_ID',
							message: 'Known Error, being handled.',
							data: false
						})
					}
				})
			} catch(e) {
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					data: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				data: false
			})
		}
	},

	



};
