/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 * @TODO: implement the user authentication
 */
var moment = require('moment')
var jwt = require('jsonwebtoken')
var CLIENT_NAME_DEFAULT = 'Not Mentioned'
var PRICE_PER_HOUR_DEFAULT = '10'
var NUMBER_OF_HOURS_DEFAULT = '0'
var TOTAL_PRICE_DEFAULT = '0'
var DETAILS = ''
var DEFAULT = ''

module.exports = {

	/**
	* @description: Send 404 code to anyone trying to access it directly
	* @TODO: Take care of authenticating the serverside
	*/
	homePage: function (req, res) {
		if (req.isAuthenticated()) {
			res.redirect('/dashboard')
		} else {
			return res.view('auth/login', { layout: 'auth/login_layout'})
		}
	},

	/**
	* @description: Send the model defaults to the app in order to view it there
	* @TODO: Take care of authenticating the serverside
	*/
	sendDefaults: function (req, res) {

		return res.json({
			clientName: CLIENT_NAME_DEFAULT,
			pricePerHour: PRICE_PER_HOUR_DEFAULT,
			numberOfHours: NUMBER_OF_HOURS_DEFAULT,
			totalPrice: TOTAL_PRICE_DEFAULT
		})

	},

	/**
	* @description: Creates new hour with optional paramaters sent by the app
	* @param: clientName {string}
	* @param: pricePerHour {string}
	* @param: numberOfHours {string}
	* @param: totalPrice {string}
	* @param: user.id {string} From the JWT
	* @TODO: Take care of parsing int for any PRICE or NUMBER!
	*/
	createHour: function (req, res) {

		var authentication = req.headers.authentication ? req.headers.authentication : null
		console.log(authentication)

		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {
					if (err) {
						console.log(err)
						return res.json({
							code: 'E_AUTHO_CRT_HOUR',
							message: 'Error occurred, While authenticating.',
							data: false
						})
					}
					var clientName = req.param('clientName') ? req.param('clientName') : CLIENT_NAME_DEFAULT
					var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : PRICE_PER_HOUR_DEFAULT
					var numberOfHours = req.param('numberOfHours') ? req.param('numberOfHours') : NUMBER_OF_HOURS_DEFAULT
					var totalPrice = req.param('totalPrice') ? req.param('totalPrice') : TOTAL_PRICE_DEFAULT
					var details = req.param('details') ? req.param('details') : TOTAL_PRICE_DEFAULT
					var chargable = req.param('chargable') ? req.param('chargable') : DEFAULT
					var priceCustom = req.param('priceCustom') ? req.param('priceCustom') : DEFAULT

					var startTime = req.param('startTime') ? req.param('startTime') : null
					var endTime = req.param('endTime') ? req.param('endTime') : null

					var user = decoded.id

					console.log(req.allParams());

					if (clientName && pricePerHour && numberOfHours && totalPrice && user && details && startTime && endTime) {

						let timeDiff = endTime - startTime
				    let duration = moment.duration(timeDiff)
				    let hours = duration.days() * 24 + duration.hours()
				    let minutes = duration.minutes()
						let seconds = duration.seconds()

						/* Processing meangifull data */
						chargable = chargable == '' ? 'No' : 'Yes'
						priceCustom = priceCustom == '' ? 'No' : 'Yes'
						let total_price = Math.floor(( ( hours * 60 + minutes ) / 60 ) * parseFloat(pricePerHour))
						/* Processing meangifull data */

						var HourModel = {
							clientName: clientName,
							pricePerHour: pricePerHour,
							numberOfHours: numberOfHours,
							totalPrice: total_price,
							details: details,
							chargable: chargable,
							priceCustom: priceCustom,
							startTime: startTime,
							endTime: endTime,
							user: user
						}

						Hour.create(HourModel, function (err, data) {
							if (err) {
								return res.json({
									message: 'Sorry, something went wrong, and it is totaly Omar\'s fault. I am going to teach him a lesson he will never forget' + err,
									data: false
								})
							}
							return res.json({
								message: 'Data has been saved.',
								data: data
							})
						})

					} else {
						return res.json({
							message: 'Sorry, you did not fill the whole fields.',
							data: false
						})
					}
				})
			} catch(err) {
				console.log(err)
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					data: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				data: false
			})
		}
	},

	/**
	* @description: Respond to a GET request from the app and send all the hours created by the current user
	* @TODO: Take care of parsing int for any PRICE or NUMBER!
	*/
	getHours: function (req, res) {

		Hour.find(function (err, data) {
			if (err) {
				return res.json({
					message: 'Sorry, something went wrong, and it is totaly Omar\'s fault. I am going to teach him a lesson he will never forget',
					data: false
				})
			}
			return res.json(data)
		})

	},

	getHours_by_user: function (req, res) {
		var authentication = req.headers.authentication ? req.headers.authentication : null

		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {
					if (err) {
						console.log(err)
						return res.json({
							code: 'E_AUTHO',
							message: 'Error occurred, While authenticating.',
							data: false
						})
					}
					var user = decoded.id
					Hour.find({user: user}, function (err, data) {
						if (err) {
							return res.json({
								message: 'Sorry, something went wrong, and it is totaly Omar\'s fault. I am going to teach him a lesson he will never forget',
								data: false
							})
						}
						return res.json(data)
					})
				})
			} catch (e) {
				console.log(err)
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					data: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				data: false
			})
		}
	},

	getUserStats: function (req, res) {
		var authentication = req.headers.authentication ? req.headers.authentication : null

		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {
					if (err) {
						console.log('---------------------------')
						console.log('AUTH_ERROR_GET_USER_STATS')
						console.log('---------------------------')
						console.log(err)
						console.log('---------------------------')
						console.log('AUTH_ERROR_GET_USER_STATS')
						console.log('---------------------------')
						return res.json({
							code: 'E_AUTHO',
							message: 'Error occurred, While authenticating.',
							data: false
						})
					}

					let user_id = decoded.id

					let TOTAL_CLIENTS = 0

					let TOTAL_HOURS = 0
					let TOTAL_PRICE = 0
					let TOTAL_PER_CLIENT = []

					Company.find({user: user_id}, function(err, companies) {
						if (err) {
							return res.json({
								code: 'E_STATS_COMPANY',
								message: 'Sorry, something went wrong, and it is totaly Omar\'s fault. I am going to teach him a lesson he will never forget',
								data: false
							})
						}

						TOTAL_CLIENTS = companies.length

						Hour.find({user: user_id}, function(err, hours) {
							if (err) {
								return res.json({
									code: 'E_STATS_HOUR',
									message: 'Sorry, something went wrong, and it is totaly Omar\'s fault. I am going to teach him a lesson he will never forget',
									data: false
								})
							}

							var hours_total = 0
							var price_total = 0
							for (var i=0 ; i<hours.length ; i++) {
								hours_total += parseInt(hours[i].numberOfHours)
								price_total += parseInt(hours[i].totalPrice)
							}

							TOTAL_HOURS = hours_total
							TOTAL_PRICE = price_total

							for (var i=0 ; i<companies.length ; i++) {
								var company_name = companies[i].companyName
								var total_hours = 0
								var total_price = 0

								for (var j=0 ; j<hours.length ; j++) {
									if (company_name === hours[j].clientName) {
										total_hours += parseInt(hours[j].numberOfHours)
										total_price += parseInt(hours[j].totalPrice)
									}
								}

								TOTAL_PER_CLIENT.push({
									clientName: company_name,
									totalPrice: total_price,
									totalHours: total_hours
								})
							}
							var data = {
								total_clients: TOTAL_CLIENTS,
								total_price: TOTAL_PRICE,
								total_hours: TOTAL_HOURS,
								total_per_client: TOTAL_PER_CLIENT
							}
							return res.json({
								data: data
							})
						})
					})




				})
			} catch (e) {
				console.log(err)
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					data: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				data: false
			})
		}
	},




};
