/**
 * AuthController
 *
 * @description :: Server-side logic for managing auths
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var jwt = require('jsonwebtoken')
var passport = require('passport')

function _onPassportAuth(req, res, err, user, info) {
	if (err) {
		return res.send(err)
	}
	if (!user) {
		return res.unauthorized(null, info && info.code, info && info.message)
	}
	user.name = user.firstName
	var outgoingToken = jwt.sign({
		user_id: user.id.toString(),
		custom: user
	}, sails.config.jwtSettings.secret)
	var url = req.query.redirect_uri +
						'&token=' + encodeURIComponent(outgoingToken) +
						'&state=' + encodeURIComponent(req.query.state)
	return res.redirect(url)
	/*return res.ok({
		token: CipherService.createToken(user)
	})*/
}

function _onPassportCustomAuth(req, res, err, user, info) {
	if (err) {
		return res.status(500).send(err)
	}
	if (!user) {
		return res.status(401).unauthorized(null, info && info.code, info && info.message)
	}
	return res.ok({
		token: CipherService.createToken(user.toJSON())
	})
}

function _onPassportServerAuth(req, res, err, user, info) {
	if (err) {
		console.log('(-) _onPassportServerAuth', err, '(-) _onPassportServerAuth')
		return res.view('messages/error', {
			code: 'E_AUTH',
			message: 'Error, something happened while logging you in..'
		})
	}
	if (!user) {
		/*return res.unauthorized(null, info && info.code, info && info.message)*/
		return res.status(401).view('messages/error', {
			code: 'E_NAUTH',
			message: 'Sorry this user doesn\'t exists in our database'
		})
	}
	req.logIn(user, function(err) {
    if (err) res.send(err)
		if(info.type === 'client') {
			req.session.user_type = null
			req.session.user_id = null

			req.session.client = true
			req.session.client_id = user.id

			return res.redirect('/client/home')
			// Must Prompt user to change his password if the random generated password still exists
			// if (user.randomPassword !== null || user.randomPassword !== '') {
			//
			// }


		} else {
			req.session.client = false
			req.session.client_id = null

			req.session.user_id = user.id
			req.session.user_type = user.type
		}

		console.log('------------------------------')
		console.log('User Name: ', (user.companyName ? user.companyName : user.firstName ? user.firstName : 'NULL --> CHECK IT OUT (CHK)'))
		console.log('user_type? ' + req.session.user_type)
		console.log('user_id? ' + req.session.user_id)
		console.log('client? ' + req.session.client)
		console.log('client_id? ' + req.session.client_id)
		console.log('------------------------------')


    return res.redirect('/dashboard')
  })

}

module.exports = {

	signin: function(req, res) {
		try {
			// Are we authenticating with Ioinc cloud services !?
			if (req.query.redirect_uri && req.query.state && req.query.token) {
				var incomingToken = jwt.verify(req.query.token, sails.config.jwtSettings.secret, function(err, decoded){
					if (err) {
						return res.status(401).send('ERROR')
					}
					req.query.email = decoded.data.email
					req.query.password = decoded.data.password
					/*
					// Need to know what bind() do in order to pass the user object to be authenticated
					*/
					passport.authenticate('local', _onPassportAuth.bind(this, req, res))(req, res)
				})
			} else {
				if (req.param('email') && req.param('password')) {
					passport.authenticate('local', _onPassportCustomAuth.bind(this, req, res))(req, res)
				} else {
					return res.status(406).json({
						message: 'Error, No params were sent..',
						code: 'EAUTH_FEW_PARAMS'
					})
				}
			}
		} catch (ex) {
			console.log('Error in jwt: ' + ex)
			return res.status(401).send('JWT Error')
		}
	},

	loginServer: function(req, res) {
		var email = req.param('email') ? req.param('email') : null
		var password = req.param('password') ? req.param('password') : null

		if (email && password) {
			passport.authenticate('local', _onPassportServerAuth.bind(this, req, res))(req, res)
		} else {
			return res.view('messages/error', {
				code: 'E_TFP',
				message: 'Error, No parameters were sent.'
			})
		}
	},

	signup: function(req, res) {
		var username = req.param('username') ? req.param('username') : null
		var password = req.param('password') ? req.param('password') : null
		var firstName = req.param('firstName') ? req.param('firstName') : null
		var email = req.param('email') ? req.param('email') : null

		if (username && password && firstName && email ) {
			var userModel = {
				provider: "local",
				username: username,
				password: password,
				firstName: firstName,
				email: email
			}

			User.findOne({email: email}).exec(function(err, user){
				if (err){
					return res.json({
						code: 'E_REGISTER_FIND_DB',
						token: false,
						message: 'Error while finding user: ' + err
					})
				}
				if (!user){
					User.create(userModel, function(err, user){
						if (err) {
							return res.json({
								code: 'E_REGISTER_CREATE_DB',
								token: false,
								message: 'Error while creating new user: ' + err
							})
						}
						else {
							return res.json({
								code: 'S_REGISTER_SUCCESS',
								message: 'User created successfully.',
								token: CipherService.createToken(user)
							})
						}
					})
				}
				else{
					return res.json({
						code: 'E_ALREADY_REGISTERD',
						message: 'User already exists',
						token: false
					})
				}
			})
		} else {
			return res.json({
				code: 'E_REGISTER_EMPTY_CRED',
				token: false,
				message: 'Error there are missing credentials'
			})
		}
	},

	refreshToken: function(req, res) {
		var authentication = req.headers.authentication ? req.headers.authentication : null


		if (authentication) {
			try {
				jwt.verify(authentication, sails.config.jwtSettings.secret, function (err, decoded) {

					var errorName = err.name ? err.name : err
					errorName = errorName ? 'TokenExpiredError' : err

					if (errorName !== 'TokenExpiredError') {
						return res.json({
							code: 'E_AUTHO_REFSH_TOKEN',
							message: 'Error occurred, While authenticating.',
							token: false
						})
					}
					decoded = jwt.decode(authentication)
					/*check if more than 5 days, force login*/
					if (decoded.iat - new Date() > 5) {
						return res.json({
							code: 'TOKEN_EXP',
							message: 'Token has been expired due to security reasons, you have to login again.',
							token: false
						})
					}

					/*Check if user already exists in our DB*/
					User.findOne({email: decoded.email}, function(err, user) {
						if (err) {
							return res.json({
								code: 'E_TOKEN_USER_FIND',
								message: 'Error, while authenticating user.',
								token: false
							})
						}
						if (!user) {
							return res.json({
								code: 'E_USER_NOT_FOUND',
								message: 'There is no user in our database with such email: ' + decoded.email,
								token: false
							})
						} else {
							var newToken = CipherService.createToken(user.toJSON())
							return res.json({
								code: 'REFSHD_TOKEN',
								message: 'Token has been refreshed, you are now authenticated.',
								token: newToken
							})
						}
					})


				})
			} catch (e) {
				return res.json({
					code: 'E_AUTHO',
					message: 'Error occurred, Probably not authorized.',
					token: false
				})
			}
		} else {
			return res.json({
				code: 'E_AUTHO_EMPTY',
				message: 'Error occurred, No Authorization was sent.',
				token: false
			})
		}
	},

	logoutServer: function(req, res) {
		req.logout()
		req.session.user_id = null
		return res.redirect('/')
	},

	show_client_password: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			if (id) {
				Company.findOne({id: id}, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_AUTH_SHOWPASSWORD',
							message: 'Error getting password, it is totaly our fault. We are fixing it right now.'
						})
					}
					return res.json({
						password: company.randomPassword
					})
				})
			} else {
				return res.view('messages/error', {
					code: 'E_AUTH_SHOWP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	}

};
