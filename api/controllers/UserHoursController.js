/**
 * UserHoursController
 *
 * @description :: Server-side logic for managing userhours
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	add_hour_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			var clientName 		= req.param('clientName') ? req.param('clientName') : null
			var pricePerHour 	= req.param('pricePerHour') ? req.param('pricePerHour') : null
			var numberOfHours = req.param('numberOfHours') ? req.param('numberOfHours') : null
			var chargable 		= req.param('chargable') ? req.param('chargable') : null
			var details 			= req.param('details') ? req.param('details') : null
			var customPrice 	= req.param('customPrice') ? true : false

			console.log(req.allParams());

			if (id && clientName && numberOfHours && details || pricePerHour) {

				var totalPrice = parseInt(pricePerHour) * parseInt(numberOfHours)

				if (!chargable){
					totalPrice = 0
					chargable = false
				} else {
					chargable = true
				}


				let hourModel = {
					clientName: clientName,
					pricePerHour: pricePerHour,
					numberOfHours: numberOfHours,
					chargable: chargable,
					totalPrice: totalPrice,
					details: details,
					priceCustom: customPrice,
					user: id
				}

				Hour.create(hourModel, function(err, hour) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_AHP',
							message: 'Error creating new hour, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'New hour has been added successfully')
					return res.redirect('/dashboard/hours?id=' + id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_AHP_PMS',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	edit_hour_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var user_id = req.param('user_id') ? req.param('user_id') : null

			var clientName = req.param('clientName') ? req.param('clientName') : null
			var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : null
			var numberOfHours = req.param('numberOfHours') ? req.param('numberOfHours') : null
			var chargable = req.param('chargable') ? req.param('chargable') : null
			var details = req.param('details') ? req.param('details') : null

			if (id && user_id && clientName && pricePerHour && numberOfHours && details) {

				if (!chargable) {
					chargable = false
				} else {
					chargable = true
				}

				let hourModel = {
					clientName: clientName,
					pricePerHour: pricePerHour,
					numberOfHours: numberOfHours,
					chargable: chargable,
					details: details,
					user: user_id
				}

				Hour.update({id: id}, hourModel, function(err, hour) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_EHP',
							message: 'Error updating hour, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', clientName + '\' hour has been updated successfully.')
					return res.redirect('/dashboard/hours?id=' + user_id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_EHP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	remove_hour_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var user_id = req.param('user_id') ? req.param('user_id') : null

			if (id && user_id) {
				Hour.destroy({id: id}, function(err, hour) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_RHP',
							message: 'Error removing client, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'Hour has been removed successfully.')
					return res.redirect('/dashboard/hours?id=' + user_id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_RHP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	get_hour_by_id: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Hour.findOne({id: id}, function(err, hour) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_GHBI',
							message: 'Error fetching hour, it is totaly our fault. We are fixing it right now.'
						})
					}
					return res.json(hour)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_GHBI_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	get_hour_user: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				User.findOne({id: id}, function(err, user) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_GHU',
							message: 'Error fetching user, it is totaly our fault. We are fixing it right now.'
						})
					}
					return res.json(user)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_GHU_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

};
