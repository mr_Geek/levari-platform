/**
 * UserClientsController
 *
 * @description :: Server-side logic for managing userclients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var randomstring = require("randomstring")

module.exports = {

	add_client_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var companyName = req.param('companyName') ? req.param('companyName') : null
			var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : null
			var email = companyName.toLowerCase().trim().replace(/\s/g,'') + '@levarilaw.com'
			var password = randomstring.generate({
				length: 20,
				charset: 'alphanumeric'
			})

			if (id && companyName && pricePerHour && password && email) {
				let companyModel = {
					companyName: companyName,
					pricePerHour: pricePerHour,
					password: password,
					randomPassword: password,
					email: email,
					user: id
				}

				Company.create(companyModel, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CP',
							message: 'Error Creating new client, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'New client has been added successfully')
					return res.redirect('/dashboard/clients?id=' + id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_ACP_PMS',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	edit_client_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var user_id = req.param('user_id') ? req.param('user_id') : null
			var companyName = req.param('companyName') ? req.param('companyName') : null
			var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : null

			if (id && companyName && pricePerHour) {
				let companyModel = {
					companyName: companyName,
					pricePerHour: pricePerHour,
					user: user_id
				}

				Company.update({id: id}, companyModel, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CU',
							message: 'Error updating client, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'Client ' + companyName + ' has been updated successfully.')
					return res.redirect('/dashboard/clients?id=' + user_id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_ECP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	remove_client_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var user_id = req.param('user_id') ? req.param('user_id') : null

			if (id && user_id) {
				Company.destroy({id: id}, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_GCBI',
							message: 'Error removing client, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'Client has been removed successfully.')
					return res.redirect('/dashboard/clients?id=' + user_id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_RCP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	get_client_by_id: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Company.findOne({id: id}, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_GCBI',
							message: 'Error fetching client, it is totaly our fault. We are fixing it right now.'
						})
					}
					return res.json(company)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_GCBI_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},


};
