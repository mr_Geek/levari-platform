/**
 * ClientController
 *
 * @description :: Server-side logic for managing clients
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


	client_update_profile: function(req, res) {
		if (req.isAuthenticated()) {
			let id = req.param('id') ? req.param('id') : null
			let contact_mail = req.param('contact_mail') ? req.param('contact_mail') : null
			let companyName = req.param('companyName') ? req.param('companyName') : null

			if (id && companyName || contact_mail) {
				let clientModel = {
					contact_mail: contact_mail,
					companyName: companyName
				}

				Company.update({id: id}, clientModel, function(err, clients) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_UUP',
							message: 'Error updating your account, it is totally our fault. We are fixing it now.'
						})
					}
					req.addFlash('success', 'Your account was updated successfully')
					return res.redirect('/client/account?id=' + id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_TFP_UUP',
					message: 'Error updating your account. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	client_update_password: function(req, res) {
		if (req.isAuthenticated()) {
			let id = req.param('id') ? req.param('id') : null
			let current_password = req.param('current_password') ? req.param('current_password') : null
			let new_password = req.param('new_password') ? req.param('new_password') : null
			let confirm_password = req.param('confirm_password') ? req.param('confirm_password') : null

			if (id && current_password && new_password && confirm_password) {
				Company.findOne({id: id}, function(err, user) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_FOU_UP',
							message: 'Error while authenticating your profile, it is totally our fault. We are fixing it now.'
						})
					}
					/*Check his current password*/
					if (!CipherService.comparePassword(current_password, user)) {
						return res.view('messages/error', {
							code: 'E_PNM_UP',
							message: 'Error this is not your current password, please try again.'
						})
					}
					/*Check his new passwords*/
					if (new_password !== confirm_password) {
						return res.view('messages/error', {
							code: 'E_PNMT_UP',
							message: 'Error passwords are not matched, please try again.'
						})
					}
					/*No problem?! Okay .. UPDATE IT*/
					Company.update({id: id}, {password: new_password}, function(err, user) {
						if (err) {
							return res.view('messages/error', {
								code: 'E_UPU_UP',
								message: 'Error while updating your password, it is totally our fault. We are fixing it now.'
							})
						}
						req.addFlash('success', 'Your password was updated successfully')
						return res.redirect('/client/account?id=' + id)
					})
				})
			} else {
				return res.view('messages/error', {
					code: 'E_TFP_UP',
					message: 'Error please fill up all the fields.'
				})
			}
		} else {
			return res.forbidden()
		}
	},




};
