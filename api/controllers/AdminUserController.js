/**
 * AdminUserController
 *
 * @description :: Server-side logic for managing adminusers
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var randomstring = require("randomstring")

module.exports = {

	remove_user_process: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type === 'admin') {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				User.destroy({id: id}, function(err, hour) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_RHP',
							message: 'Error removing user, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'Lawyer has been removed successfully.')
					return res.redirect('/dashboard/all-users')
				})
			} else {
				return res.view('messages/error', {
					code: 'E_RHP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	/**
		* Adds new user used by platform admin only
		* @method add_user
		* @param {Object} obj - some req
		* @param {Object} obj - some res
		* @return {json} message indicates whether user has been added or not
	**/
	add_user: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type === 'admin') {
			let id = req.param('id') ? req.param('id') : null

			let email = req.param('email') ? req.param('email') : null
			let firstName = req.param('firstName') ? req.param('firstName') : null
			let username = req.param('username') ? req.param('username') : null
			let password = req.param('password') ? req.param('password') : null
			let passwordConfirm = req.param('passwordConfirm') ? req.param('passwordConfirm') : null
			let type = req.param('type') ? req.param('type') : null
			let typeFinal = ''

			if(type) {
				typeFinal = 'admin'
			} else {
				typeFinal = 'user'
			}

			if (id && password && passwordConfirm && email && firstName && username) {

				// Check if user already exists
				User.find({username: username}, function(err, users) {
					if (err) {
						return res.view('messages/error', {
							// status: 'ERROR',
							code: 'USER_ADM_FIND',
							message: 'An error occurred while creating the user, contact adminstrator with error code to fix it.'
						})
					}
					if (users.length === 0) {
						// Check if passwords does not match
						if (password !== passwordConfirm) {
							return res.view('messages/error', {
								// status: 'ERROR',
								code: 'PASSWORD_NMATCH',
								message: 'Your password and confirmation password do not match'
							})
						}
						// Okay he is a good user create him!!
						var userModel = {
							email: email,
							firstName: firstName,
							username: username,
							password: password,
							type: typeFinal
						}
						User.create(userModel, function(err, user) {
							if (err) {
								return res.view('messages/error', {
									// status: 'ERROR',
									code: 'USER_ADM_CREATE',
									message: 'An error occurred while creating the user, contact adminstrator with error code to fix it.'
								})
							}
							// return res.json({
							// 	status: 'SUCCESS',
							// 	code: 'USER_CREATED',
							// 	message: '<strong>' + firstName + '</strong> has been created successfully.'
							// })
							req.addFlash('success', '<strong>' + firstName + '</strong> has been created successfully.')
							return res.redirect('/dashboard/all-users')
						})
					} else {
						return res.view('messages/error', {
							// status: 'ERROR',
							code: 'USER_EXISTS',
							message: 'A User with this username already exists in our database.'
						})
					}
				})
			} else {
				return res.view('messages/error', {
					// status: 'ERROR',
					code: 'ADM_USER_CREATE',
					message: 'Please fill up all the fields.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	/**
		* Same as the add_client_process action in UserClientsController but it redirects to the admin page
		* @method add_user
		* @param {Object} obj - some req
		* @param {Object} obj - some res
		* @return {json} message indicates whether user has been added or not
	**/
	add_client_process: function(req, res) {
		if (req.isAuthenticated()) {
			var id = req.param('id') ? req.param('id') : null
			var companyName = req.param('companyName') ? req.param('companyName') : null
			var pricePerHour = req.param('pricePerHour') ? req.param('pricePerHour') : null
			var email = companyName.toLowerCase().trim().replace(/\s/g,'').replace(/[^a-zA-Z0-9 ]/g, "") + '@levarilaw.com'
			var password = randomstring.generate({
				length: 20,
				charset: 'alphanumeric'
			})

			if (id && companyName && pricePerHour && password && email) {
				let companyModel = {
					companyName: companyName,
					pricePerHour: pricePerHour,
					password: password,
					randomPassword: password,
					email: email,
					user: id
				}

				Company.create(companyModel, function(err, company) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CP',
							message: 'Error Creating new client, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'New client has been added successfully')
					return res.redirect('/dashboard/all-clients?id=' + id)
				})
			} else {
				return res.view('messages/error', {
					code: 'E_ACP_PMS',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	remove_client_process: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type === 'admin') {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Company.destroy({id: id}, function(err, hour) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_RHP',
							message: 'Error removing user, it is totaly our fault. We are fixing it right now.'
						})
					}
					req.addFlash('success', 'Client has been removed successfully.')
					return res.redirect('/dashboard/all-clients')
				})
			} else {
				return res.view('messages/error', {
					code: 'E_RHP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

	more_details_process: function(req, res) {
		if (req.isAuthenticated() && req.session.user_type === 'admin') {
			var id = req.param('id') ? req.param('id') : null

			if (id) {
				Hour.find({user: id}, function(err, hours) {
					if (err) {
						return res.view('messages/error', {
							code: 'E_CP',
							message: 'Error retrieving user hours, it is totaly our fault. We are fixing it right now.'
						})
					}
					Company.find({user: id}, function(err, companies){
						if (err) {
							return res.view('messages/error', {
								code: 'E_CP',
								message: 'Error retrieving user companies, it is totaly our fault. We are fixing it right now.'
							})
						}
						return res.view('dashboard/admin/more-details', {
							data: hours,
							companies: companies
						})
					})
				})
			} else {
				return res.view('messages/error', {
					code: 'E_RHP_PM',
					message: 'Error delivering data to the server. Most likely it is server side fault, we on it.'
				})
			}
		} else {
			return res.forbidden()
		}
	},

};
