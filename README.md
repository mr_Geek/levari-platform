# Levari Client/Internal Platform
----------------
Works as an *API for Levari Tracking Hours Hybrid App*, and an online plaform for *Levari's clients* to check and watch their lawyers progress, and *Levari's partners/admin staff* to watch over lawyers as well.

### Made with sails
based on Node Js framework Express.js the MVC framework is used to develop Levari Platform Server & API

