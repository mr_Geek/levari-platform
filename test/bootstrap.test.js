var sails = require('sails')

before(done => {

	// this.timeout(1000 * 5)

	sails.lift({
		// configuration for testing
	}, err => {
		if (err)
			return done(err)

		done(err, sails)
	})
})

after(done => {
	// Clearing things out

	sails.lower(done)
})